-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2015 at 07:21 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `ambulance`
--

CREATE TABLE IF NOT EXISTS `ambulance` (
  `license_plate_no` varchar(45) NOT NULL,
  `fpid` varchar(45) NOT NULL,
  `service_provider_id` int(50) NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`license_plate_no`),
  KEY `focal_idx` (`fpid`),
  KEY `FK_serviceproviders` (`service_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ambulance`
--

INSERT INTO `ambulance` (`license_plate_no`, `fpid`, `service_provider_id`, `lat`, `lng`, `status`) VALUES
('d192', 'fp06', 1, 33.6518, 73.0762, 0),
('d2085', 'fp07', 1, NULL, NULL, 0),
('fsd2322', 'fp09', 1, NULL, NULL, 0),
('gh173', 'fp02', 1, NULL, NULL, 0),
('isb9687', 'fp04', 1, NULL, NULL, 0),
('kbp0954', 'fp01', 1, 33.5953, 73.051, 0),
('kch2985', 'fp05', 1, NULL, NULL, 0),
('kmn2897', 'fp10', 1, NULL, NULL, 0),
('lhr9879', 'fp08', 1, NULL, NULL, 0),
('rwp2877', 'fp03', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `caller_log`
--

CREATE TABLE IF NOT EXISTS `caller_log` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `contact_no` bigint(11) DEFAULT NULL,
  `location` varchar(45) NOT NULL,
  `logdate` date NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `caller_log`
--

INSERT INTO `caller_log` (`cid`, `name`, `contact_no`, `location`, `logdate`) VALUES
(1, 'Shahid Aneeq', 3247689456, 'NUST', '2015-03-23'),
(2, 'Laiba Mushtaq ', 3462389067, 'Satellite town', '2015-04-10'),
(3, 'Hamza Daniyal', 3225645321, 'Bahria Town', '2015-04-12'),
(4, 'Nazir Bukhsh', 3135467341, 'Street 10, G-10', '2015-04-12'),
(5, 'Pervaiz Elahi', 3148745632, 'Street 6, F-11', '2015-04-13'),
(6, 'Shareef Chaudhry', 3125786908, 'Street 9, F-11', '2015-04-15'),
(7, 'Hafeez Sheikh', 3245646890, 'Street 5, E-8', '2015-04-16'),
(8, 'Dua Malik', 3137895467, 'Street 4, F-9', '2015-04-20'),
(9, 'Ahsan Gulrez', 3215987689, 'Street 8, F-7', '2015-04-22'),
(10, 'Sami Abdullah', 3025673490, 'Street 2, G-9', '2015-04-23');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_log`
--

CREATE TABLE IF NOT EXISTS `emergency_log` (
  `emergency_id` int(50) NOT NULL AUTO_INCREMENT,
  `emergency_date` datetime NOT NULL,
  `ambulance_id` varchar(50) NOT NULL,
  `caller_id` int(11) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`emergency_id`),
  KEY `FK_ambulanceid` (`ambulance_id`),
  KEY `FK_callerid` (`caller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `emergency_log`
--

INSERT INTO `emergency_log` (`emergency_id`, `emergency_date`, `ambulance_id`, `caller_id`, `description`) VALUES
(1, '2015-05-14 00:00:00', 'd192', 1, 'Emergency at stadium road'),
(2, '2015-05-13 12:00:00', 'd2085', 1, 'Emergency at murree road');

-- --------------------------------------------------------

--
-- Table structure for table `focal_person`
--

CREATE TABLE IF NOT EXISTS `focal_person` (
  `fpid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `contact_no` bigint(11) NOT NULL,
  PRIMARY KEY (`fpid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `focal_person`
--

INSERT INTO `focal_person` (`fpid`, `name`, `contact_no`) VALUES
('fp01', 'Faraz Ahmed', 3152320323),
('fp02', 'Abdul Shakoor', 3247658934),
('fp03', 'Nadeem Ali', 3455952113),
('fp04', 'Tahir Gul', 3425613455),
('fp05', 'Talha Paracha', 3465987690),
('fp06', 'Abid Ali', 3214879562),
('fp07', 'Shahid Naeem', 3053422675),
('fp08', 'Ghulam Riaz', 3009856436),
('fp09', 'Muhammad Hassan', 3135762986),
('fp10', 'Jamal Khan', 3215647897);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `message_id` int(50) NOT NULL AUTO_INCREMENT,
  `operator_id` varchar(45) NOT NULL,
  `focalperson_id` varchar(45) NOT NULL,
  `contents` varchar(500) DEFAULT NULL,
  `message_time` datetime NOT NULL,
  `caller_id` int(11) DEFAULT NULL,
  `ack` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`),
  KEY `FK_oid` (`operator_id`),
  KEY `FK_mfpid` (`focalperson_id`),
  KEY `FK_mcallerid` (`caller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `operator_id`, `focalperson_id`, `contents`, `message_time`, `caller_id`, `ack`) VALUES
(1, 'op01', 'fp01', 'Don''t go to Lalazar. It was a fake call.', '2015-05-15 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `operator`
--

CREATE TABLE IF NOT EXISTS `operator` (
  `oid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `contact_no` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operator`
--

INSERT INTO `operator` (`oid`, `name`, `contact_no`) VALUES
('op01', 'Kaleem ullah', '03425647897'),
('op02', 'Asad Tahir', '03086754980'),
('op03', 'Adeel Butt', '03147878909'),
('op04', 'Habeeb Murtaza', '03125643589');

-- --------------------------------------------------------

--
-- Table structure for table `service_providers`
--

CREATE TABLE IF NOT EXISTS `service_providers` (
  `service_provider_id` int(50) NOT NULL AUTO_INCREMENT,
  `service_provider_name` varchar(100) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `vehicles_count` int(50) NOT NULL,
  PRIMARY KEY (`service_provider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service_providers`
--

INSERT INTO `service_providers` (`service_provider_id`, `service_provider_name`, `contact_no`, `vehicles_count`) VALUES
(1, 'Edhi', '3232322', 55),
(4, 'Chhipa', '0513939283', 20);

-- --------------------------------------------------------

--
-- Table structure for table `user_credentials`
--

CREATE TABLE IF NOT EXISTS `user_credentials` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL DEFAULT 'operator',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_credentials`
--

INSERT INTO `user_credentials` (`username`, `password`, `user_type`) VALUES
('12besefrehman', 'ninja25', 'operator'),
('12besemshahid', 'ninja18', 'admin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ambulance`
--
ALTER TABLE `ambulance`
  ADD CONSTRAINT `FK_fpid` FOREIGN KEY (`fpid`) REFERENCES `focal_person` (`fpid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_serviceproviders` FOREIGN KEY (`service_provider_id`) REFERENCES `service_providers` (`service_provider_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `focal` FOREIGN KEY (`fpid`) REFERENCES `focal_person` (`fpid`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `emergency_log`
--
ALTER TABLE `emergency_log`
  ADD CONSTRAINT `FK_ambulanceid` FOREIGN KEY (`ambulance_id`) REFERENCES `ambulance` (`license_plate_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_callerid` FOREIGN KEY (`caller_id`) REFERENCES `caller_log` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `FK_mcallerid` FOREIGN KEY (`caller_id`) REFERENCES `caller_log` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mfpid` FOREIGN KEY (`focalperson_id`) REFERENCES `focal_person` (`fpid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_oid` FOREIGN KEY (`operator_id`) REFERENCES `operator` (`oid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
