<?php
/**
 * This page serves as the RESTful API front to our application.
 *
 * Team Social Ninjas
 * SEECS Social Hackathon 2015
 */

require 'vendor/autoload.php';

/**
 * Some constants
 */
define("HTTP_OK"			, 200);
define("HTTP_OK_NO_CONTENT"	, 204);
define("HTTP_BAD_REQUEST"	, 400);
define("HTTP_UNAUTHORIZED"	, 401);
define("HTTP_NOT_FOUND"		, 404);

// Create a new app
$app = new \Slim\Slim();
$app->setName('ambuloc');

/**
 * Authentication middleware
 */

session_start();

// Checks whether user is logged in
function authenticate(\Slim\Route $route) {
	$app = \Slim\Slim::getInstance();
	if(!isset($_SESSION['username'])) {
		$app->halt(HTTP_UNAUTHORIZED);
	}
}

/**
 * Database connectivity helpers
 */

// Connect to database
function connectDb() {
	$db_server='localhost:3306';
	$db_user='root';
	$db_pass='root';
	$db_database='ems';
	$db = new PDO('mysql:host='.$db_server.'; dbname='.$db_database,$db_user,$db_pass);
	if(!$db) {
		$app = \Slim\Slim::getInstance();
		$app->halt(500);
	}
	return $db;
}

/**
 * API Spec 1.1
 */
$app->post('/login', function () use ($app) { //check login credentials
	// Always set response type to json
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();

    // If user is logged in...

		// ... else try validating user using db.
        //PLEASE USE FORM SUBMITTION INSTEAD OF AJAX
		// POST requests receive arguments as json which has to be decoded.
		try {
			$request = $app->request();
			$body = $request->getBody();
			$input = json_decode($body);
            
			$statement = $db->prepare("SELECT username,user_type from user_credentials where (username=? and password=?)");
	    	// Run query with given parameters
			$statement->execute(array($input->un, $input->pwd));
            // Fetch all results as an associative arrays
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	    	// If there was a result (username, password was correct...)
			if($statement->rowCount() == 1) {
		    	// Login successful!
				$_SESSION['username'] = $result[0]['username'];
				echo json_encode(array('username'=>$result[0]['username'],'user_type'=>$result[0]['user_type']));
				// WARNING! Response data not decided yet!
			} else {
		    	// Login failed. Respond with error
				$app->response()->status(HTTP_UNAUTHORIZED);
			}
		} catch (Exception $e) {
	    	// Some other error, maybe due to incomplete data
			$app->response()->status(HTTP_BAD_REQUEST);
			$app->response()->header('X-Status-Reason', $e->getMessage());
		}
	
});

$app->delete('/login', 'authenticate', function () use ($app) { //logout 
	// Note that the authenticate function was called above ...
	// ... We need to be logged in before we logout :P
	$app->response()->header('Content-Type', 'application/json');
	unset($_SESSION['user_id']);
	session_destroy();
	$app->response()->status(HTTP_OK_NO_CONTENT);
});
$app->get('/adminoperator', 'authenticate', function () use ($app) {
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("SELECT *FROM operator");
    	// Run query with given parameters
		$statement->execute();
    	// Fetch all results as an associative arrays
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
    	// If there was a result (username, password was correct...)
		if($statement->rowCount() >= 1) {
	    	// Login successful!
	    	while($row = $result->fetch_assoc())
						$data[] = array('oid'=>$result[0]['oid'],'name'=>$result[0]['name'],'contact_no'=>$result[0]['contact_no']);
			echo json_encode(array($data));
			// WARNING! Response data not decided yet!
		} else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->post('/admin/addoperator','authenticate', function () use ($app) //admin add operator
{
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);

		$statement = $db->prepare("INSERT INTO operator VALUES (?,?,?)");
		$statement->execute($input->oid,$input->oname,$input->ocontactno);
		$r_id=$db->lastInsertId();

		$select=$db->prepare("SELECT oid from operator where oid=?");
        $select->execute(array($r_id));
		$result = $select->fetchAll(PDO::FETCH_ASSOC);
		
		if($select->rowCount() == 1) {
                    echo json_encode($result[0]);
		}
		else {
	    	$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});


$app->post('/admin/updateoperator/:operatorid', 'authenticate', function ($operatorid) use ($app) { //update operator
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("UPDATE operator SET contact_no=? WHERE oid=?");
    	// Run query with given parameters
		$statement->execute(array($input->contactno,$operatorid));
    	// Fetch all results as an associative arrays
		$select=$db->prepare("SELECT * from operator where oid=?");
		$select->execute(array($operatorid));
		$result = $select->fetchAll(PDO::FETCH_ASSOC);
	    	// If there was a result (username, password was correct...)
		if($select->rowCount() == 1) {
		    	// User profile exists

			echo json_encode(array('contact_no'=>$result[0]['contact_no']));

		} else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->delete('/admin/operator/:operatorid', 'authenticate', function ($operatorid) use ($app) { //deleteoperator
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	
		$statement = $db->prepare("DELETE from operator WHERE oid=?");
    	$statement->execute(array($operatorid));
    	$delete= $db->prepare("SELECT * from operator where oid=?");
        $delete->execute(array($operatorid));
        $result=$delete->fetchAll(PDO::FETCH_ASSOC);
        if($delete->rowCount() == 0) {
		    	$app->response()->status(HTTP_OK_NO_CONTENT); 
        }
        else {
	
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->get('/allserviceprovider', 'authenticate', function () use ($app) { //allserviceprovider
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("SELECT service_provider_name,contact_no,vehicles_count FROM service_providers");
    	// Run query with given parameters
		$statement->execute();
    	// Fetch all results as an associative arrays
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
    	if($statement->rowCount() >= 1) {
    		echo json_encode($result); 
		}
    	// If there was a result (username, password was correct...)
		else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->get('/serviceprovider', 'authenticate', function () use ($app) { //oneserviceprovider
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("SELECT service_provider_id FROM service_providers WHERE service_provider_name=?");
    	// Run query with given parameters
		$statement->execute($input->sname);
    	// Fetch all results as an associative arrays
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
    	// If there was a result (username, password was correct...)
		if($statement->rowCount() == 1) {
	    	// Login successful!
	    	echo json_encode($result);
			// WARNING! Response data not decided yet!
		} else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->post('/addserviceprovider','authenticate', function () use ($app) // add service provider
{
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("INSERT INTO service_providers (service_provider_name,contact_no,vehicles_count) VALUES (?,?,?)");
    	// Run query with given parameters
		$statement->execute(array($input->service_provider_name,$input->contactno,$input->vehiclescount));
    	$r_id=$db->lastInsertId();
    	
		$check=$db->prepare("SELECT service_provider_id from service_providers where user_id=? ");
    	$check->execute(array($r_id));
    	$checkRow=$check->fetchAll(PDO::FETCH_ASSOC);
	    	
		if($check->rowCount() == 1)
		{
	    	echo json_encode($checkRow[0]);
			
		} else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->delete('/serviceprovider/delete/:serviceproviderid','authenticate', function ($serviceproviderid) use ($app) // delete service provider
{
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("DELETE FROM service_providers WHERE service_provider_id=?");
    	// Run query with given parameters
		$statement->execute($serviceproviderid);
    	$delete= $db->prepare("SELECT * from service_providers where service_provider_id=?");
        $delete->execute(array($serviceproviderid));
        $result=$delete->fetchAll(PDO::FETCH_ASSOC);
        if($delete->rowCount() == 0) {
		    	$app->response()->status(HTTP_OK_NO_CONTENT); 
        }
        else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->post('/update/service_provider/:serviceproviderid','authenticate', function ($serviceproviderid) use ($app) // update service provider contact no
{
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("UPDATE service_providers SET contact_no=? WHERE service_provider_id=?");
    	// Run query with given parameters
		$statement->execute(array($input->contactno,$input->serviceproviderid));
    	$select=$db->prepare("SELECT * from service_providers where service_proider_id=?");
		$select->execute(array($serviceproviderid));
		$result = $select->fetchAll(PDO::FETCH_ASSOC);
    	// If there was a result (username, password was correct...)
		if($select->rowCount() >= 1) {
	    	echo json_encode(array('contactno'=>$result[0]['contact_no']));
		} else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});

$app->post('/update/service_provider/:serviceproviderid','authenticate', function () use ($app) // update service provider contact no
{
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("UPDATE service_providers SET vehicles_count=? WHERE service_provider_id=?");
    	// Run query with given parameters
		$statement->execute(array($input->vehiclescount,$input->serviceproviderid));
    	$select=$db->prepare("SELECT * from service_providers where service_proider_id=?");
		$select->execute(array($serviceproviderid));
		$result = $select->fetchAll(PDO::FETCH_ASSOC);
    	// If there was a result (username, password was correct...)
		if($select->rowCount() >= 1) {
	    	echo json_encode(array('vehiclescount'=>$result[0]['vehicles_count']));
		} else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}


});

$app->post('/update/service_provider/:serviceproviderid','authenticate', function ($serviceproviderid) use ($app) // update service provider name
{
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();
	try {
		$request = $app->request();
		$body = $request->getBody();
		$input = json_decode($body);
    	// Prepare query
		$statement = $db->prepare("UPDATE service_providers SET service_provider_name=? WHERE service_provider_id=?");
    	// Run query with given parameters
		$statement->execute(array($input->serviceprovidername,$serviceproviderid));
    	$select=$db->prepare("SELECT * from service_providers where service_proider_id=?");
		$select->execute(array($serviceproviderid));
		$result = $select->fetchAll(PDO::FETCH_ASSOC);
    	// If there was a result (username, password was correct...)
		if($select->rowCount() >= 1) {
	    	echo json_encode(array('serviceprovidername'=>$result[0]['service_provider_name']));
		} else {
	    	// Login failed. Respond with error
			$app->response()->status(HTTP_NOT_FOUND);
		}
	} catch (Exception $e) {
    	// Some other error, maybe due to incomplete data
		$app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}

});





/* Shozib's Part - Start */


$app->post('/emergencylog','authenticate', function () use ($app) { //check login credentials
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();

        try {
        $request = $app->request();
        $body = $request->getBody();
        $input = json_decode($body);

        $statement = $db->prepare("SELECT emergency_date,(SELECT service_provider_name from service_providers where service_provider_id IN (SELECT service_provider_id FROM ambulance WHERE license_plate_no=?)) AS service_provider,(SELECT name from caller_log WHERE cid=?) AS caller,description FROM emergency_log");
        $statement->execute(array($input->license_plate_no,$input->cid));

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $i = 0;
        while($i < $statement->rowCount())
        {
            $arr[$i] = array('emergency_date'=>$result[$i]['emergency_date'],'service_provider'=>$result[$i]['service_provider'], 'caller'=>$result[$i]['caller'], 'description'=>$result[$i]['description']);
            
            $i++;
        }
        echo json_encode($arr);

    }
    catch (Exception $e)
    {
        $app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
    }

});


$app->get('/checkambulanceavailability','authenticate', function () use ($app) { //check login credentials
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();

        try {
        $request = $app->request();
        $body = $request->getBody();
        $input = json_decode($body);

        $statement = $db->prepare("SELECT license_plate_no FROM ambulance where status = '0'");
        $statement->execute();

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $i = 0;
        while($i < $statement->rowCount())
        {
            $arr[$i] = array('license_plate_no'=>$result[$i]['license_plate_no']);
            $i++;
        }
        if($statement->rowCount() >= 1)
        {
            echo json_encode($arr);
        }
        else
        {
            echo json_encode(array('status'=>FALSE));
        }
    }
    catch (Exception $e)
    {
        $app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
    }

});

$app->get('/admin/viewlog','authenticate', function () use ($app) { //check login credentials
	$app->response()->header('Content-Type', 'application/json');
	$db = connectDb();

        try {
        $statement = $db->prepare("SELECT * FROM emergency_log");
        $statement->execute();

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $i = 0;
        while($i < $statement->rowCount())
        {
            $arr[$i] = array('emergency_date'=>$result[$i]['emergency_date'],'ambulance_id'=>$result[$i]['ambulance_id'], 'caller_id'=>$result[$i]['caller_id'], 'description'=>$result[$i]['description']);
            
            $i++;
        }
        echo json_encode($arr);

    }
    catch (Exception $e)
    {
        $app->response()->status(HTTP_BAD_REQUEST);
		$app->response()->header('X-Status-Reason', $e->getMessage());
    }

});

/* Shozib's Part - End */

$app->run();

?>