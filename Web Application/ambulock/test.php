<?php
/**
 * This page serves as the RESTful API front to our application.
 *
 * Team Social Ninjas
 * SEECS Social Hackathon 2015
 */

require 'vendor/autoload.php';

/**
 * Some constants
 */
define("HTTP_OK"			, 200);
define("HTTP_OK_NO_CONTENT"	, 204);
define("HTTP_BAD_REQUEST"	, 400);
define("HTTP_UNAUTHORIZED"	, 401);
define("HTTP_NOT_FOUND"		, 404);

// Create a new app
$app = new \Slim\Slim();
$app->setName('ambuloc');

/**
 * Authentication middleware
 */

session_start();

// Checks whether user is logged in
function authenticate(\Slim\Route $route) {
	$app = \Slim\Slim::getInstance();
	if(!isset($_SESSION['username'])) {
		$app->halt(HTTP_UNAUTHORIZED);
	}
}

/**
 * Database connectivity helpers
 */

// Connect to database
function connectDb() {
	$db_server='localhost:3306';
	$db_user='root';
	$db_pass='root';
	$db_database='ems';
	$db = new PDO('mysql:host='.$db_server.'; dbname='.$db_database,$db_user,$db_pass);
	if(!$db) {
		$app = \Slim\Slim::getInstance();
		$app->halt(500);
	}
	return $db;
}

/**
 * API Spec 1.1
 */
$app->post('/login', function () use ($app) { //check login credentials

    $app->response()->header('Content-Type', 'application/json');
    $db = connectDb();

	try {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $statement = $db->prepare("SELECT username,user_type from user_credentials where (username=? and password=?)");
	    	// Run query with given parameters
			$statement->execute(array($username, $password));
            // Fetch all results as an associative arrays
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	    	// If there was a result (username, password was correct...)
			if($statement->rowCount() == 1) {
		    	// Login successful!
				$_SESSION['username'] = $result[0]['username'];
				echo json_encode(array('username'=>$result[0]['username'],'user_type'=>$result[0]['user_type']));
				// WARNING! Response data not decided yet!
			} else {
		    	// Login failed. Respond with error
				$app->response()->status(HTTP_UNAUTHORIZED);
			}

    }
    catch (Exception $e) {
	    	// Some other error, maybe due to incomplete data
			$app->response()->status(HTTP_BAD_REQUEST);
			$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});


$app->run();

?>