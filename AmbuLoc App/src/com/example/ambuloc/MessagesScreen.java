package com.example.ambuloc;

import java.util.ArrayList;
import java.util.List;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MessagesScreen extends Activity {

	public class message {
		public String opname;
		public String focalpersonname;
		public String topic;
		public String content;

		message(String name1, String name2, String top, String cont) {
			opname = name1;
			focalpersonname = name2;
			topic = top;
			content = cont;
		}
	}

	public class gps_location {
		public double latitude;
		public double longitude;

		public void setter(double x, double y) {
			latitude = x;
			longitude = y;
		}
	}

	messageAdapter adapter;
	List<message> messageList;
	ListView list;
	TextView tName;
	AlertDialog setStatusAlert, sendMessageAlert;
	gps_location current_location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messages_screen);

		list = (ListView) findViewById(R.id.mList);
		current_location= new gps_location();

		messageList = new ArrayList<message>();
		message m = new message("System", "Hamza", "Handle Emergency", "Area: G-10 Islamabad.");
		messageList.add(m);
		
		m = new message("Rashid", "Hamza", "Additional Information", "Patient has heart problem. Hurry up");
		messageList.add(m);
		
		m = new message("System", "Kaleem Ullah", "Handle Emergency", "Area: Satellite Town, B Block");
		messageList.add(m);
		
		m = new message("System", "Ghulam Riaz", "Handle Emergency", "Area: H-11.");
		messageList.add(m);

		adapter = new messageAdapter();
		list.setAdapter(adapter);
		
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
				message msg = adapter.getWhichSelected(arg2);
				Intent intent= new Intent(MessagesScreen.this,MessageDetailScreen.class);
				intent.putExtra("topic", msg.topic);
				intent.putExtra("sender", msg.opname);
				intent.putExtra("content", msg.content);
				startActivity(intent);
			}
		});

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();
		final View x;
		x = inflater.inflate(R.layout.setstatus, null);
		dialog.setView(x);
		dialog.setTitle("Set Status");

		dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface d, int which) {
				// TODO Auto-generated method stub

				d.dismiss();
			}
		});

		dialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface d, int which) {
						// TODO Auto-generated method stub
						d.cancel();
					}
				});

		AlertDialog.Builder dialog2 = new AlertDialog.Builder(this);
		LayoutInflater inflater2 = getLayoutInflater();
		final View xy;
		xy = inflater2.inflate(R.layout.sendmessage, null);
		dialog.setView(xy);
		dialog.setTitle("Send Message to operator");

		dialog2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface d, int which) {
				// TODO Auto-generated method stub

				d.dismiss();
			}
		});

		dialog2.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface d, int which) {
						// TODO Auto-generated method stub
						d.cancel();
					}
				});

		setStatusAlert = dialog.create();
		sendMessageAlert = dialog2.create();
		
//		final Handler handler = new Handler();
//
//		Runnable updateData;
//		updateData= new Runnable()
//		{
//		    public void run(){
//		         //call the service here
//		    	current_location=calcCoordinates();
//		    	Toast.makeText(MessagesScreen.this,"Location:"+current_location.latitude+current_location.longitude,Toast.LENGTH_SHORT).show();
//		         ////// set the interval time here
//		         handler.postDelayed(updateData,5000);
//		    }
//		};
		
		new android.os.Handler().postDelayed(
			    new Runnable() 
			    {
			        public void run()
			        {
			        	current_location=calcCoordinates();
				    	Toast.makeText(MessagesScreen.this,"Location:"+current_location.latitude+current_location.longitude,Toast.LENGTH_SHORT).show();
			        }
			    },300);

	}

	public void startStatus() {
		sendMessageAlert.show();
	}

	public void startMessage() {
		setStatusAlert.show();
	}

	public gps_location calcCoordinates() {
		// Get the location manager
		double lat, lon;
		gps_location gps = new gps_location();
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String bestProvider = locationManager.getBestProvider(criteria, false);
		Location location = locationManager.getLastKnownLocation(bestProvider);
		try
		{
			lat = location.getLatitude();
			lon = location.getLongitude();
		}
		catch(Exception e)
		{
			lat=-1.0;
			lon=-1.0;
		}
		gps.setter(lat, lon);
		return gps;

	}

	// public List<message> getMessagesForListView() {
	//
	// return messageList;
	// }

	public class messageAdapter extends BaseAdapter {

		// List<message> mList = getMessagesForListView();

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return messageList.size();
		}

		@Override
		public message getItem(int arg0) {
			// TODO Auto-generated method stub
			return messageList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(final int arg0, View arg1, ViewGroup arg2) {

			// TODO Auto-generated method stub
			if (arg1 == null) {
				LayoutInflater inflater = (LayoutInflater) MessagesScreen.this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				arg1 = inflater.inflate(R.layout.comment, arg2, false);
			}

			message msg = messageList.get(arg0);
			tName= (TextView) arg1.findViewById(R.id.topic);
			// Toast.makeText(MessagesScreen.this,
			// "Value of topic is:"+msg.topic,Toast.LENGTH_SHORT).show();
			tName.setText(msg.topic);

			return arg1;
		}

		public message getWhichSelected(int position) {
			return messageList.get(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.messages_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		// noinspection SimplifiableIfStatement
		if (id == R.id.setStatus) {
			startStatus();
			return true;
		} else if (id == R.id.sendMessage) {
			startMessage();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

}
