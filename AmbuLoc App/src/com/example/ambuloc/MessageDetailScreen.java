package com.example.ambuloc;

import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MessageDetailScreen extends Activity {

	AlertDialog setStatusAlert,sendMessageAlert;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_detail_screen);
		
		String sender,message,topic;
		Intent intent= getIntent();
		topic= intent.getStringExtra("topic");
		sender= intent.getStringExtra("sender");
		message= intent.getStringExtra("content");
		
		getActionBar().setTitle(topic);
		
		TextView textSend= (TextView) findViewById(R.id.sender);
		TextView textCont= (TextView) findViewById(R.id.content);
		
		textSend.setText(sender);
		textCont.setText(message);
		

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();
		final View x;
		x = inflater.inflate(R.layout.setstatus, null);
		dialog.setView(x);
		dialog.setTitle("Set Status");
		
		dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface d, int which) {
				// TODO Auto-generated method stub
				
				d.dismiss();
			}
		});

		dialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface d, int which) {
						// TODO Auto-generated method stub
						d.cancel();
					}
				});
		
		
		AlertDialog.Builder dialog2 = new AlertDialog.Builder(this);
		LayoutInflater inflater2 = getLayoutInflater();
		final View xy;
		xy = inflater2.inflate(R.layout.sendmessage, null);
		dialog.setView(xy);
		dialog.setTitle("Send Message to operator");
		
		dialog2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface d, int which) {
				// TODO Auto-generated method stub
				
				d.dismiss();
			}
		});

		dialog2.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface d, int which) {
						// TODO Auto-generated method stub
						d.cancel();
					}
				});

		setStatusAlert = dialog.create();
		sendMessageAlert = dialog2.create();
	}
	
	public void startStatus()
	{
		sendMessageAlert.show();
	}
	
	public void startMessage()
	{
		setStatusAlert.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.message_detail_screen, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		// noinspection SimplifiableIfStatement
		if (id == R.id.setStatus) 
		{
			startStatus();
			return true;
		} 
		else if (id == R.id.sendMessage) {
			startMessage();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

}
