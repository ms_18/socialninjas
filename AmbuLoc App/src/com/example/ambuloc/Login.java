package com.example.ambuloc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Login extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		final EditText un = (EditText) findViewById(R.id.username);
		final EditText pwd = (EditText) findViewById(R.id.pwd);
		ImageButton signIn = (ImageButton) findViewById(R.id.signIn);

		signIn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// send values of username and pwd to server
				new Thread() {
					public void run() {
						JSONObject jsonobj = new JSONObject();
						try 
						{
							jsonobj.put("username", un.getText().toString());
							jsonobj.put("password", pwd.getText().toString());
							
							DefaultHttpClient httpclient = new DefaultHttpClient();
							HttpPost httppostreq = new HttpPost("http://192.168.1.2:8989/ambuloc/index.php/login");
							
							StringEntity se = new StringEntity(jsonobj.toString());
							se.setContentType("application/json;charset=UTF-8"); 
							se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
							httppostreq.setEntity(se);
							try {
								HttpResponse httpresponse = httpclient.execute(httppostreq);
								String responseText = EntityUtils.toString(httpresponse.getEntity());
								JSONObject json = new JSONObject(responseText);
								Toast.makeText(Login.this, "Response: "+json.toString(), Toast.LENGTH_SHORT).show();
							} catch (ClientProtocolException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} 
						catch (JSONException e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}.start();

				Intent intent = new Intent(Login.this, MessagesScreen.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
