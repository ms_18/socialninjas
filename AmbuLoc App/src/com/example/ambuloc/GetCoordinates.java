package com.example.ambuloc;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ambuloc.JSONParser;

public class GetCoordinates extends Activity implements LocationListener

{
    private TextView textlat, textlong;
    private Button button, btn_tracker;
    private LocationManager myLoc;
    JSONParser jParser = new JSONParser();
    private static String url_all_products = "http://192.168.56.1/AmbuLoc/android_connect/send_coords.php";
    private static final String TAG_SUCCESS = "success";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_coordinates);

        textlat = (TextView) findViewById(R.id.textView2);
        textlong = (TextView) findViewById(R.id.textView3);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view)

            {
				new SendCoordinates().execute();
            }
        });

        myLoc = (LocationManager) getSystemService(getApplicationContext().LOCATION_SERVICE);
        LocationListener LocLis = this;
        myLoc.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, LocLis);

        Location loc = myLoc.getLastKnownLocation(LocationManager.GPS_PROVIDER);

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_get_coordinates, menu);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location!=null)
        {
            double plong = location.getLongitude();
            double plat = location.getLatitude();
            textlat.setText(Double.toString(plat));
            textlong.setText(Double.toString(plong));
        }


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    

    public class SendCoordinates extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {
            //return null;

            List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("longitude",textlong.getText().toString() ));
            params.add(new BasicNameValuePair("latitude",textlat.getText().toString() ));


            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "POST", params);

            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
				return json.toString();	

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }


}
